//
//  CartView.swift
//  CartIndicator
//
//  Created by sameh on 9/10/17.
//  Copyright © 2017 Radvy. All rights reserved.
//

import Foundation
import UIKit

public enum ActionType { case plus,minus }

public protocol CartControllerDelegate:class
{
    func CartDidChange(operationType:ActionType,value:Int)
}

public class CartController:UIView
{
  @IBOutlet weak var  holder:UIView!
   @IBOutlet weak var plus:UIButton!
   @IBOutlet weak var minus:UIButton!
   @IBOutlet weak var indicator:UILabel!
    public weak var delegate:CartControllerDelegate?
    
    public var maxValue:Int?
    public var minValue:Int =  0
    
    @IBInspectable public var borderColor:UIColor?{
        didSet{
            holder.layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable public var labelColor:UIColor?
    {
        didSet{
            indicator?.textColor = labelColor
        }
    }

    @IBInspectable public var buttonsColor:UIColor?
        {
        didSet
        {
            plus.setTitleColor(buttonsColor, for: .normal)
            minus.setTitleColor(buttonsColor, for: .normal)
        }
    }
    
    @IBInspectable public var plusImage:UIImage?{
        didSet{
            plus.setImage(self.plusImage, for: .normal)
            plus.setTitle(nil, for: .normal)
        }
    }
    
    @IBInspectable public var minusImage:UIImage?{
        didSet{
            minus.setImage(self.minusImage, for: .normal)
            minus.setTitle(nil, for: .normal)
        }
    }
    
    @IBInspectable public var plusText:String?{
        didSet{
            plus.setImage(nil, for: .normal)
            plus.setTitle(plusText, for: .normal)
        }
    }
    
    @IBInspectable public var minusText:String?{
        didSet{
            minus.setImage(nil, for: .normal)
            minus.setTitle(minusText, for: .normal)
        }
    }
    
    public var font:UIFont?
    {
        didSet
        {
            indicator?.font = font
            plus.titleLabel?.font = font
            minus.titleLabel?.font = font
        }
    }
    
    public var value:Int = 0
    {
        didSet
        {
            indicator?.text = String(value)
//            sendActions(for: .valueChanged)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        
        initalizing()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
        initalizing()
    }
    
    private func initalizing() {
        
        
        let bundle = Bundle(for: CartController.self)
        bundle.loadNibNamed("CartView", owner: self, options: nil)
        addSubview(holder)
        holder.frame = self.bounds
        holder.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        
        //Plus Sign
    
        plus.contentVerticalAlignment = .center
            plus.contentHorizontalAlignment = .center
           plus.imageView?.contentMode = .scaleAspectFit
            plus.setTitleColor(buttonsColor, for: .normal)
        indicator?.textAlignment = .center
        indicator?.text = String(value)
        
        
        //Minus Sign
      
            minus.imageView?.contentMode = .scaleAspectFit
            minus.setTitleColor(buttonsColor, for: .normal)
       
        //Setting intial Value
    }
    
     @IBAction  func addValue(_ sender : UIButton)
    {
        guard maxValue == nil else
        {
            if maxValue! >= value
            {
                return
            }
            else
            {
                value += 1
                delegate?.CartDidChange(operationType: .plus, value: value)
            }
            
            return
        }
        
        value += 1
        delegate?.CartDidChange(operationType: .plus, value: value)
    }
    
     @IBAction  func subtractValue(_ sender : UIButton)
    {
        if value <=  minValue
        {
            return
        }
        else
        {
            value -= 1
            delegate?.CartDidChange(operationType: .minus, value: value)
        }
    }
}
